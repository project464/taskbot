package main

import (
	"context"
	"go.uber.org/zap"
	"huetaskis/internal/dataproviders/sqliteprovider"
	"huetaskis/internal/dataproviders/telegram"
	"huetaskis/internal/entrypoints"
	"huetaskis/internal/usecases"
	"time"
)

var (
	ctx              context.Context
	dbProvider       sqliteprovider.IPostgresProvider
	telegramProvider telegram.ITelegramApiProvider
	useCases         usecases.IUseCases
	entryPoints      entrypoints.IEntrypoints
	logger           zap.SugaredLogger
)

func init() {
	dbProvider = sqliteprovider.NewPostgresProvider(ctx, logger)
	telegramProvider = telegram.NewTelegramApiProvider(ctx, logger)
	useCases = usecases.NewUseCases(ctx, dbProvider, telegramProvider, logger)
	entryPoints = entrypoints.NewEntrypoints(ctx, dbProvider, telegramProvider, useCases, logger)

}

func main() {
	dbProvider.InitDB()

	listenerChan := make(chan bool)
	existChan := make(chan bool)

	//Проверяем подключение к telegram
	go func(exitChan chan bool) {
		defer func() {
			existChan <- true
		}()
		err := telegramProvider.Reconnector()
		if err != nil {
			logger.Error(err)
		}
	}(listenerChan)

	//Публикуем по времени
	go func(exitChan chan bool) {
		defer func() {
			existChan <- true
		}()
		time.Sleep(time.Second * 5)
		go entryPoints.IncomingMessage()
	}(listenerChan)

	<-listenerChan
}
