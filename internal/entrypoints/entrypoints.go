package entrypoints

import (
	"context"
	"go.uber.org/zap"
	"huetaskis/internal/dataproviders/sqliteprovider"
	"huetaskis/internal/dataproviders/telegram"
	"huetaskis/internal/usecases"
)

type Entrypoints struct {
	ctx    context.Context
	db     sqliteprovider.IPostgresProvider
	tg     telegram.ITelegramApiProvider
	uc     usecases.IUseCases
	logger zap.SugaredLogger
}

type IEntrypoints interface {
	IncomingMessage()
}

func NewEntrypoints(
	ctx context.Context,
	database sqliteprovider.IPostgresProvider,
	telegram telegram.ITelegramApiProvider,
	usecases usecases.IUseCases,
	sugaredlogger zap.SugaredLogger,
) IEntrypoints {
	return &Entrypoints{
		ctx:    ctx,
		db:     database,
		tg:     telegram,
		uc:     usecases,
		logger: sugaredlogger,
	}
}

func (ep *Entrypoints) IncomingMessage() {

	ep.uc.Listening(ep.tg.IncomingMessageHandler())

}
