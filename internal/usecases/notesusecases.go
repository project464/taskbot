package usecases

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"huetaskis/internal/dataproviders/sqliteprovider"
	"huetaskis/utils"
	"log"
	"strconv"
	"time"
)

func (uc *UseCases) CleanTaskList(update tgbotapi.Update) {
	list := uc.listTasks(update.Message.Chat.ID)
	var activeTasks = 0
	for _, n := range *list {
		if !n.Done {
			activeTasks += 1
		}
	}
	switch activeTasks {
	case 0:
		for _, task := range *list {
			_, err := uc.db.DeleteTask(&task)
			if err != nil {
				log.Println(err)
			}
		}
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.CleanIsDone)
		err := uc.tg.SendMessageHandler(msg)
		if err != nil {
			log.Println(err)
		}

	default:
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.HasUndoneTasks)
		err := uc.tg.SendMessageHandler(msg)
		if err != nil {
			log.Println(err)
		}
	}

}
func (uc *UseCases) DeleteTask(update tgbotapi.Update) {
	if update.Message.CommandArguments() == "" {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.EnterTaskNumber)
		err := uc.tg.SendMessageHandler(msg)
		if err != nil {
			log.Println(err)
		}
	} else {
		id, err := strconv.Atoi(update.Message.CommandArguments())
		if err != nil {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.EnterTaskNumber)
			err := uc.tg.SendMessageHandler(msg)
			if err != nil {
				log.Println(err)
			}
		} else {
			list, err := uc.db.GetTasks(&sqliteprovider.Task{ChatId: update.Message.Chat.ID})
			deleted := false
			for _, t := range *list {
				if t.ID == uint(id) {
					_, err := uc.db.DeleteTask(&sqliteprovider.Task{
						ID:     uint(id),
						ChatId: update.Message.Chat.ID,
					})
					if err != nil {
						log.Println(err)
					}

					msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.DeleteTaskMessage(id))
					err = uc.tg.SendMessageHandler(msg)
					if err != nil {
						log.Println(err)
					}
					deleted = true
				}
			}
			if !deleted {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.ThereIsNoSuchTask)
				err = uc.tg.SendMessageHandler(msg)
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
}
func (uc *UseCases) DoneTask(update tgbotapi.Update) {
	if update.Message.CommandArguments() == "" {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.EnterTaskNumber)
		err := uc.tg.SendMessageHandler(msg)
		if err != nil {
			log.Println(err)
		}
	} else {
		id, err := strconv.Atoi(update.Message.CommandArguments())
		if err != nil {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.EnterTaskNumber)
			err := uc.tg.SendMessageHandler(msg)
			if err != nil {
				log.Println(err)
			}
		} else {
			list, err := uc.db.GetTasks(&sqliteprovider.Task{ChatId: update.Message.Chat.ID})
			updated := false
			for _, t := range *list {
				if t.ID == uint(id) {
					switch t.Done {
					case true:
						msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.TaskAlreadyDone)
						err = uc.tg.SendMessageHandler(msg)
						if err != nil {
							log.Println(err)
						}
						updated = true
					case false:

						_, err = uc.db.UpdateTask(&sqliteprovider.Task{
							ID:     uint(id),
							ChatId: update.Message.Chat.ID,
							Done:   true,
						})
						if err != nil {
							log.Println(err)
						}
						msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.DoneTaskMessage(id))
						err = uc.tg.SendMessageHandler(msg)
						if err != nil {
							log.Println(err)
						}
						updated = true
					}
				}

			}
			if !updated {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.ThereIsNoSuchTask)
				err = uc.tg.SendMessageHandler(msg)
				if err != nil {
					log.Println(err)
				}
			}
		}
	}
	list := uc.listTasks(update.Message.Chat.ID)
	var activeTasks = 0
	for _, n := range *list {
		if !n.Done {
			activeTasks += 1
		}
	}
	if activeTasks == 0 {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.Congratulation)
		err := uc.tg.SendMessageHandler(msg)
		if err != nil {
			log.Println(err)
		}
	}
}
func (uc *UseCases) ArchivedTasks(update tgbotapi.Update) {
	notes := uc.listTasks(update.Message.Chat.ID)
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, prettify(notes))
	err := uc.tg.SendMessageHandler(msg)
	if err != nil {
		log.Println(err)
	}
}

func (uc *UseCases) ListTasks(update tgbotapi.Update) {
	var activeNotes []sqliteprovider.Task
	notes := uc.listTasks(update.Message.Chat.ID)
	for _, note := range *notes {
		if !note.Done {
			activeNotes = append(activeNotes, note)
		}
	}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, prettify(&activeNotes))
	err := uc.tg.SendMessageHandler(msg)
	if err != nil {
		log.Println(err)
	}
}

func (uc *UseCases) NewTask(update tgbotapi.Update) {
	if update.Message.CommandArguments() == "" {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.EnterTaskDescription)
		err := uc.tg.SendMessageHandler(msg)
		if err != nil {
			log.Println(err)
		}
	} else {
		_, err := uc.db.CreateTask(&sqliteprovider.Task{
			Time:        time.Now().Format(utils.AppConfig.Webserver.TimeFormat),
			ChatId:      update.Message.Chat.ID,
			Description: update.Message.CommandArguments(),
		})
		if err != nil {
			log.Println(err)
		}

		data, err := uc.db.GetTasks(&sqliteprovider.Task{ChatId: update.Message.Chat.ID})
		if err != nil {
			log.Println(err)
		}

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, prettify(data))
		err = uc.tg.SendMessageHandler(msg)
		if err != nil {
			log.Println(err)
		}
	}
}

func prettify(n *[]sqliteprovider.Task) string {
	s := "Список задач:\n"
	for _, d := range *n {
		if d.Done {
			s += fmt.Sprint("[*]\t", d.ID, " - ", d.Description, "\n")
		} else {
			s += fmt.Sprint("[ ]\t", d.ID, " - ", d.Description, "\n")
		}
	}
	return s
}

func (uc *UseCases) listTasks(owner int64) *[]sqliteprovider.Task {
	notes, err := uc.db.GetTasks(&sqliteprovider.Task{ChatId: owner})
	if err != nil {
		log.Println(err)
	}
	log.Println("User ", owner, " has ", len(*notes))
	return notes
}
