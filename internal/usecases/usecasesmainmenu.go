package usecases

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"go.uber.org/zap"
	"huetaskis/internal/dataproviders/sqliteprovider"
	"huetaskis/internal/dataproviders/telegram"
	"huetaskis/utils"
)

type UseCases struct {
	ctx    context.Context
	db     sqliteprovider.IPostgresProvider
	tg     telegram.ITelegramApiProvider
	logger zap.SugaredLogger
}

type IUseCases interface {
	NewTask(update tgbotapi.Update)
	ListTasks(update tgbotapi.Update)
	DeleteTask(update tgbotapi.Update)
	DoneTask(update tgbotapi.Update)
	ArchivedTasks(update tgbotapi.Update)
	Listening(channel tgbotapi.UpdatesChannel)
	Help(channel tgbotapi.Update)
	CommandNotFound(channel tgbotapi.Update)
}

func NewUseCases(
	ctx context.Context,
	database sqliteprovider.IPostgresProvider,
	telegram telegram.ITelegramApiProvider,
	sugaredlogger zap.SugaredLogger,
) IUseCases {
	return &UseCases{
		ctx:    ctx,
		db:     database,
		tg:     telegram,
		logger: sugaredlogger,
	}
}

func (uc *UseCases) Listening(channel tgbotapi.UpdatesChannel) {
	for update := range channel {
		uc.logger.Info(update.UpdateID)
		uc.logger.Info(update.Message.Chat.ID)
		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case "start":
				uc.CreateChatTable(update)
			case "help":
				uc.Help(update)
			case "delete":
				uc.DeleteTask(update)
			case "list":
				uc.ListTasks(update)
			case "new":
				uc.NewTask(update)
			case "done":
				uc.DoneTask(update)
			case "archive":
				uc.ArchivedTasks(update)
			case "clean":
				uc.CleanTaskList(update)
			default:
				uc.CommandNotFound(update)
			}
		}
	}

}
func (uc *UseCases) CreateChatTable(update tgbotapi.Update) {
	err := uc.db.CreateChatTaskTable(&sqliteprovider.Task{ChatId: update.Message.Chat.ID})
	if err != nil {
		uc.logger.Error(err)
	}
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.Welcome)
	err = uc.tg.SendMessageHandler(msg)
	if err != nil {
		uc.logger.Error(err)
	}
}

func (uc *UseCases) CommandNotFound(update tgbotapi.Update) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.CommandNotFound)
	err := uc.tg.SendMessageHandler(msg)
	if err != nil {
		uc.logger.Error(err)
	}
}

func (uc *UseCases) Help(update tgbotapi.Update) {
	msg := tgbotapi.NewMessage(update.Message.Chat.ID, utils.CommandList)
	err := uc.tg.SendMessageHandler(msg)
	if err != nil {
		uc.logger.Error(err)
	}
}
