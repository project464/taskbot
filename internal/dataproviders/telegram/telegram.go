package telegram

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"go.uber.org/zap"
	"huetaskis/utils"
	"time"
)

var conn *tgbotapi.BotAPI

type TelegramApiProvider struct {
	ctx    context.Context
	logger zap.SugaredLogger
}

type ITelegramApiProvider interface {
	Reconnector() error
	IncomingMessageHandler() tgbotapi.UpdatesChannel
	SendMessageHandler(message tgbotapi.MessageConfig) error
}

func NewTelegramApiProvider(
	ctx context.Context,
	sugaredlogger zap.SugaredLogger,
) ITelegramApiProvider {
	return &TelegramApiProvider{
		ctx:    ctx,
		logger: sugaredlogger,
	}
}

func (it *TelegramApiProvider) botConnector() *tgbotapi.BotAPI {
	if conn != nil {
		it.logger.Info("Telegram connector is actual")
		return conn
	} else {
		// используя токен создаем новый инстанс бота
		conn, err := tgbotapi.NewBotAPI(utils.AppConfig.Telegram.BotId)
		if err != nil {
			switch err.Error() {
			case "Not Found":
				it.logger.Info("Bot not found, disabling telegram bot")
				return nil
			default:
				it.logger.Info("telegram not connected, check internet connection:", err)
				return nil
			}
		} else {
			it.logger.Info("Authorized on account %s", conn.Self.UserName)
			return conn
		}
	}
}
func (it *TelegramApiProvider) Reconnector() error {
	for {
		it.logger.Info("Is telegram connector actual?")
		conn = it.botConnector()
		it.logger.Info("Telegram connector waits for 5 minutes")
		time.Sleep(time.Minute * 5)
	}
}

//func (it *TelegramApiProvider) UserList(update tgbotapi.Update) sqliteprovider.Users {
//	chat, err := conn.GetChatMember(tgbotapi.GetChatMemberConfig{tgbotapi.ChatConfigWithUser{}})
//	if err != nil {
//		return sqliteprovider.Users{}
//	}
//
//	return sqliteprovider.Users{}
//}

func (it *TelegramApiProvider) IncomingMessageHandler() tgbotapi.UpdatesChannel {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	return conn.GetUpdatesChan(u)
}

func (it *TelegramApiProvider) SendMessageHandler(message tgbotapi.MessageConfig) error {
	_, err := conn.Send(message)
	if err != nil {
		return err
	}
	return nil
}
