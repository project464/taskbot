package sqliteprovider

import "time"

type Chats struct {
	ChatID int64 `json:"chatid" gorm:"path"`
}

type Users struct {
	ID          uint   `json:"id" gorm:"primaryKey"`
	Time        string `json:"time" gorm:"time"`
	Owner       int64  `json:"path" gorm:"path"`
	Description string `json:"description" gorm:"path"`
	Done        bool   `json:"content" gorm:"content"`
}

type Task struct {
	ID          uint   `json:"id" gorm:"primaryKey"`
	Time        string `json:"time" gorm:"time"`
	ChatId      int64  `json:"path" gorm:"path"`
	Description string `json:"description" gorm:"path"`
	Done        bool   `json:"content" gorm:"content"`
	Someshit    string `json:"content" gorm:"content"`
}

type Reminder struct {
	ID        uint      `json:"id" gorm:"primaryKey"`
	Time      string    `json:"time" gorm:"time"`
	Starttime time.Time `json:"starttime" gorm:"starttime"`
	Endtime   time.Time `json:"endtime" gorm:"endtime"`
	Title     string    `json:"title" gorm:"name"`
	Note      string    `json:"note" gorm:"note"`
}
