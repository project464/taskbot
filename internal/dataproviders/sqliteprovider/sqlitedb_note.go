package sqliteprovider

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"gorm.io/gorm"
	"huetaskis/utils"
	"time"
)

var conn *gorm.DB

type PostgresProvider struct {
	ctx    context.Context
	logger zap.SugaredLogger
}

type IPostgresProvider interface {
	InitDB()
	CreateTask(*Task) (*Task, error)
	DeleteTask(*Task) (*Task, error)
	UpdateTask(*Task) (*Task, error)
	GetTasks(*Task) (*[]Task, error)
	GetAllNotes(*Task) (*[]Task, error)
	CreateChatTaskTable(*Task) error
}

func NewPostgresProvider(
	ctx context.Context,
	sugaredlogger zap.SugaredLogger,
) IPostgresProvider {
	return &PostgresProvider{
		ctx:    ctx,
		logger: sugaredlogger,
	}
}

func (ip *PostgresProvider) InitDB() {
	conn = initDBConn()
	migrate(conn)
}

func (ip *PostgresProvider) CreateChatTaskTable(task *Task) error {
	res := conn.Create(Chats{ChatID: task.ChatId})
	if res.Error != nil {
		return res.Error
	}
	err := conn.Table(fmt.Sprint(task.ChatId)).AutoMigrate(&Task{})
	if err != nil {
		return err
	}
	return nil
}

func (ip *PostgresProvider) CreateTask(task *Task) (*Task, error) {
	task.Time = time.Now().Format(utils.AppConfig.Webserver.TimeFormat)
	ip.logger.Info("saving")
	ip.logger.Info("conn:", conn)
	ip.logger.Info("mn:", task)
	err := conn.Table(fmt.Sprint(task.ChatId)).Create(task)
	if err.Error != nil {
		return nil, err.Error
	}
	return task, nil
}

func (ip *PostgresProvider) GetAllNotes(task *Task) (*[]Task, error) {
	var mns *[]Task
	var res *gorm.DB
	res = conn.Table(fmt.Sprint(task.ChatId)).Find(&mns)
	if res.Error != nil {
		return nil, res.Error
	}
	return mns, nil
}
func (ip *PostgresProvider) GetTasks(task *Task) (*[]Task, error) {
	var mns *[]Task
	ip.logger.Info(task)
	var res *gorm.DB
	res = conn.Table(fmt.Sprint(task.ChatId)).Find(&mns, task)
	if res.Error != nil {
		return nil, res.Error
	}
	return mns, nil
}

func (ip *PostgresProvider) UpdateTask(task *Task) (*Task, error) {
	ip.logger.Info("update")
	ip.logger.Info(task)
	res := conn.Table(fmt.Sprint(task.ChatId)).Model(&task).Updates(task)
	if res.Error != nil {
		return nil, res.Error
	}
	return task, nil
}

func (ip *PostgresProvider) DeleteTask(task *Task) (*Task, error) {
	var mn *Task
	ip.logger.Info("delete")
	ip.logger.Info(task)
	res := conn.Table(fmt.Sprint(task.ChatId)).Delete(&mn, task)
	if res.Error != nil {
		//log.Println("delete fail")
		return nil, res.Error
	}
	return mn, nil
}
