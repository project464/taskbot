package sqliteprovider

import (
	"errors"
	"gorm.io/gorm"
	"huetaskis/utils"
	"log"
	"time"
)

func (ip *PostgresProvider) CreateReminder(r Reminder) (Reminder, error) {
	r.Time = time.Now().Format(utils.AppConfig.Webserver.TimeFormat)
	log.Println("saving")
	err := conn.Create(&r)
	if err.Error != nil {
		return Reminder{}, err.Error
	}
	return r, nil
}

func (ip *PostgresProvider) GetReminder(r Reminder) ([]Reminder, error) {
	var rs []Reminder
	var res *gorm.DB
	if r.ID == 0 {
		res = conn.Find(&rs)
		if res.Error != nil {
			return nil, res.Error
		}
		return rs, nil
	}
	res = conn.First(&rs, r.ID)
	if res.Error != nil {
		return nil, res.Error
	}
	return rs, nil
}

func (ip *PostgresProvider) UpdateReminder(r Reminder) (Reminder, error) {
	log.Println("update")
	res := conn.Model(&r).Updates(r)
	if res.Error != nil {
		return Reminder{}, res.Error
	}
	return r, nil
}

func (ip *PostgresProvider) DeleteReminder(r Reminder) (Reminder, error) {
	var mr Reminder
	if r.ID == 0 {
		return Reminder{}, errors.New("Id is empty")
	}
	log.Println("delete")
	res := conn.Delete(&mr, r.ID)
	if res.Error != nil {
		log.Println("delete fail")
		return Reminder{}, res.Error
	}
	return mr, nil
}
