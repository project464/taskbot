package sqliteprovider

import (
	"fmt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"huetaskis/utils"
	"log"
)

func initDBConn() *gorm.DB {
	if conn != nil {
		return conn
	} else {
		return initDB(utils.AppConfig.Database.DbFile)
	}
}

func initDB(filepath string) *gorm.DB {
	conn, err := gorm.Open(sqlite.Open(filepath), &gorm.Config{})
	if err != nil || conn == nil {
		log.Println(err)
		log.Println(conn)
		if err != nil {
			log.Println(err)
		}
	}
	log.Println("Database Connected")
	return conn
}
func migrate(dbc *gorm.DB) {
	err := dbc.AutoMigrate(
		&Chats{},
		&Reminder{},
	)
	if err != nil {
		log.Println(err)
	}
	chtlist, err := migrateChatTables()
	for _, chat := range *chtlist {
		err := dbc.Table(fmt.Sprint(chat.ChatID)).AutoMigrate(
			&Task{},
		)
		if err != nil {
			log.Println(err)
		}
	}
}
func migrateChatTables() (*[]Chats, error) {

	var chts *[]Chats
	var res *gorm.DB
	res = conn.Find(&chts)
	if res.Error != nil {
		return nil, res.Error
	}
	return chts, nil
}
