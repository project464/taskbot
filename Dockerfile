FROM golang as build
WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
RUN go build -o /out/app /app



FROM fedora
WORKDIR /code
COPY --from=build /out/app /code/app
CMD ["/code/app"]
