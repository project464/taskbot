package utils

import (
	"path"
)

const (
	TomltemplatesPath = "configs"
	AppConfigName     = "appconfig.toml"
)

var AppConfig = (&TomlAppConfig{}).ReadTomlAppConfig(path.Join(TomltemplatesPath, AppConfigName))
