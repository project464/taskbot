package utils

import (
	"github.com/BurntSushi/toml"
	"log"
)

type TomlAppConfig struct {
	Database struct {
		DbFile string `toml:"DbFile"`
	} `toml:"database"`

	Webserver struct {
		AdressAndPort string `toml:"AdressAndPort"`
		TimeFormat    string `toml:"TimeFormat"`
	} `toml:"webserver"`

	Telegram struct {
		BotId string `toml:"botid"`
	} `toml:"telegram"`
}

func (t *TomlAppConfig) ReadTomlAppConfig(path string) TomlAppConfig {

	_, err := toml.DecodeFile(path, &t)
	if err != nil {
		log.Println(err)
	}
	return *t

}
