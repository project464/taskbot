package utils

import "fmt"

var (
	Welcome     = "Добро пожаловать! Список команд можно получить отправив команду /help"
	CommandList = `/help - помощь
/new TASK_DESCRIPTION - создание новой задачи
/delete № - удаление задачи
/done № - пометить задачу завершенной
/list - вывод активных задач
/archive - показать активные и завершенные задачи
/clean - очистить список, если все задачи выполнены`

	CommandNotFound      = "Команда не найдена, попробуйте /help"
	EnterTaskDescription = "Введите описание задачи после команды"
	EnterTaskNumber      = "Введите номер задачи после команды"
	Congratulation       = "Поздравляю! Вы сделали все дела."
	HasUndoneTasks       = "Нельзя очистить список. У вас есть незавершенные дела!"
	CleanIsDone          = "Список очищен!"
	ThereIsNoSuchTask    = "Нет такой задачи"
	TaskAlreadyDone      = "Задача уже выполнена!"
)

func DoneTaskMessage(s int) string {
	return "Задача " + fmt.Sprint(s) + " выполнена!"
}

func DeleteTaskMessage(s int) string {
	return "Задача " + fmt.Sprint(s) + " удалена"
}
